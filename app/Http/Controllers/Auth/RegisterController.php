<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    public function index()
    {
        return view('auth.register');
    }

    public function register(Request $request)
    {
        $request->validate([
            "name" => "required|min:4",
            "username" => "required|min:4",
            "email" => "email|required|unique:users,email",
            "password" => "required|min:5",
            "retype_password" => "same:password"
        ]);
        $request->request->add([
            "password" => bcrypt($request->password)
        ]);
        $user = User::create($request->all());
        Auth::login($user);
        return redirect('/')->with('success', 'berhasil Registrasi');
    }
}
