<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }
    public function login(Request $request)
    {
        $request->validate([
            "username" => 'required',
            "password" => 'required'
        ]);
        $type = filter_var($request->username, FILTER_VALIDATE_EMAIL) ? "email" : "username";
        if (!Auth::attempt([$type => $request->username, 'password' => $request->password])) {
            return back()->with('error', 'Password Salah');
        }

        return redirect('/')->with('success', 'Login Success');
    }
    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
