<?php

namespace App\Http\Controllers;

use App\Models\Guru;
use Illuminate\Http\Request;

class GuruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Guru::latest()->get();
        return view("pages.guru.index", compact(['data']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.guru.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "nama" => 'required|min:4',
            "nip" => 'required|integer|min:7',
            "jabatan" => 'required',
            "pendidikan" => 'required',
            "tempat_lahir" => 'required',
            "tanggal_lahir" => 'required',
            "agama" => 'required',
            "telp" => 'required|min:11',
            "alamat" => 'required',
            "file" => 'mimes:jpg,png,jpeg:max:1024'
        ]);
        // dd($request->all());
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $fileName = time() . "." . $file->getClientOriginalExtension();
            $file->move(public_path('images'), $fileName);
            $request->request->add(['foto' => $fileName]);
        }

        Guru::updateOrCreate(['id' => $request->id], $request->all());
        $message = !empty($request->id) ? 'data berhasil di rubah' : 'data berhasil di tambahkan';
        return redirect(route('guru.index'))->with('success', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Guru $guru)
    {
        return view('pages.guru.detail', compact(['guru']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Guru $guru)
    {
        return view('pages.guru.edit', compact(['guru']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Guru $guru)
    {
        $guru->delete();
        return back()->with('success', 'Data guru berhasil dihapus');
    }
}
