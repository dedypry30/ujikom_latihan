@extends('auth.app')
@section('content')
<div class="register-box-body">
    <p class="login-box-msg">Register a new membership</p>

    <form action="{{ route('register') }}" method="post">
        @csrf
        <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Full name" name="name" value="{{ old('name') }}">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
            @error('name')
            <div class="text-danger">{{ $message }} </div>
            @enderror
        </div>
        <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Username" name="username"
                value="{{ old('username') }}">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
            @error('username')
            <div class="text-danger">{{ $message }} </div>
            @enderror
        </div>
        <div class="form-group has-feedback">
            <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            @error('email')
            <div class="text-danger">{{ $message }} </div>
            @enderror
        </div>
        <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password" name="password"
                value="{{ old('password') }}">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            @error('password')
            <div class="text-danger">{{ $message }} </div>
            @enderror
        </div>
        <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Retype password" name="retype_password"
                value="{{ old('retype_password') }}">
            <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
            @error('retype_password')
            <div class="text-danger">{{ $message }} </div>
            @enderror
        </div>
        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox icheck">
                    <label>
                        <input type="checkbox"> I agree to the <a href="#">terms</a>
                    </label>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
            </div>
            <!-- /.col -->
        </div>
    </form>

    <a href="{{ route('login') }}" class="text-center">I already have a membership</a>
</div>
@endsection