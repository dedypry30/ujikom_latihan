@extends('layouts.index')
@section('content')
<div class="row">
    <div class="col-md-6">
        <form action="{{ route('guru.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" value="{{ $guru->id }}" name="id">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <input type="text" class="form-control" name="nama" placeholder="Nama" value="{{ $guru->nama }}">
                </div>
                @error('nama')
                <div class="text-danger">{{$message }} </div>
                @enderror
            </div>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                    <input type="text" class="form-control" name="nip" placeholder="NIP" value="{{ $guru->nip }}">
                </div>
                @error('nip')
                <div class="text-danger">{{$message }} </div>
                @enderror
            </div>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    <input type="text" class="form-control" name="jabatan" placeholder="Jabatan"
                        value="{{ $guru->jabatan }}">
                </div>
                @error('jabatan')
                <div class="text-danger">{{$message }} </div>
                @enderror
            </div>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-graduation-cap"></i></span>
                    <input type="text" class="form-control" name="pendidikan" placeholder="Pendidikan"
                        value="{{ $guru->pendidikan }}">
                </div>
                @error('pendidikan')
                <div class="text-danger">{{$message }} </div>
                @enderror
            </div>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-bank"></i></span>
                    <input type="text" class="form-control" name="tempat_lahir" placeholder="Tempat Lahir"
                        value="{{ $guru->tempat_lahir }}">
                </div>
                @error('tempat_lahir')
                <div class="text-danger">{{$message }} </div>
                @enderror
            </div>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <input type="date" class="form-control" name="tanggal_lahir" placeholder="Tanggal Lahir"
                        value="{{ $guru->tanggal_lahir }}" data-inputmask="&quot;mask&quot;:&quot;(12-12-1990)&quot;"
                        data-mask="">
                </div>
                @error('tanggal_lahir')
                <div class="text-danger">{{$message }} </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="agama">Agama</label>
                <select name="agama" id="agama" class="form-control" value="{{ $guru->agama }}">
                    <option>Islam</option>
                    <option>Kristen</option>
                    <option>Hindu</option>
                    <option>Budha</option>
                    <option>Lainnya</option>
                </select>
                @error('agama')
                <div class="text-danger">{{$message }} </div>
                @enderror
            </div>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                    <input type="text" class="form-control" name="telp" placeholder="No Telp" value="{{ $guru->telp }}"
                        data-inputmask="&quot;mask&quot;:&quot;(12-12-1990)&quot;" data-mask="">
                </div>
                @error('telp')
                <div class="text-danger">{{$message }} </div>
                @enderror
            </div>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                    <textarea name="alamat" cols="30" rows="10" class="form-control">{{ $guru->alamat }} </textarea>
                </div>
                @error('alamat')
                <div class="text-danger">{{$message }} </div>
                @enderror
            </div>
            <div class="form-group">
                <div class="input-group">
                    <label for="foto">Photo</label>
                    <input type="file" name="file" class="form-control" value="{{ $guru->foto }}">
                    <p>Silahkan input photo guru</p>
                </div>
                @if (!empty($guru->foto))
                <img src="{{ asset('images/'.$guru->foto) }}" alt="" width="300">
                @endif
                @error('file')
                <div class="text-danger">{{$message }} </div>
                @enderror
            </div>
            <div>
                <a href="{{ route('guru.index') }}" class="btn btn-warning btn-sm">Batal</a>
                <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
            </div>
        </form>
    </div>
</div>
@endsection