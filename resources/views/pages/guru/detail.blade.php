@extends('layouts.index')
@section('content')
<div class="row">
    <div class="col-md-3">
        <div class="box box-primary">

            <div class="box-body box-profile">
                <img src="{{ asset('images/'.$guru->foto) }}" alt="" class="profile-user-img img-responsive img-circle">
                <h3 class="profile-username text-center"> {{ $guru->name }} </h3>
                <p>
                <h4 class="text-muted text-center">{{ $guru->jabatan }} </h4>
                </p>
                <strong><i class="fa fa-book margin-r-5">NIP :</i></strong> {{ $guru->nip }}
                <hr>
                <strong><i class="fa fa-book margin-r-5">Pendidikan :</i></strong> {{ $guru->pendidikan }}
                <hr>
                <strong><i class="fa fa-book margin-r-5">Tempat Lahir :</i></strong> {{ $guru->tempat_lahir }}
                <hr>
                <strong><i class="fa fa-book margin-r-5">Tanggal Lahir :</i></strong> {{ $guru->tanggal_lahir }}
                <hr>
                <strong><i class="fa fa-book margin-r-5">No Telp :</i></strong> {{ $guru->telp }}
                <hr>
                <strong><i class="fa fa-book margin-r-5">Alamat :</i></strong> {{ $guru->alamat }}
                <hr>
                <a href="/guru" class="btn btn-success btn-block"><b>CLOSE</b></a>
            </div>
        </div>
    </div>
</div>
@endsection