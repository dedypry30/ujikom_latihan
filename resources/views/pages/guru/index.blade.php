@extends('layouts.index')
@section('content')
<div class="box">
    <div class="container">
        <h3 class="box-title"> <i class="fa fa-database"> Data Guru</i></h3>
        <div class="box-header">
            <a href="{{ route('guru.create') }} " class="btn btn-primary btn-sm pull-right"><i
                    class="fa fa-plus-square"> Tambah Guru</i></a>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="tabelGuru" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>NIP</th>
                    <th>Alamat</th>
                    <th>Photo</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $key => $guru)
                <tr>
                    <td>{{ $key+1 }} </td>
                    <td>{{ $guru->nama }} </td>
                    <td>{{ $guru->nip }} </td>
                    <td>{{ $guru->alamat }} </td>
                    <td>
                        <img src="{{ asset('images/'.$guru->foto) }}" alt="" width="60">
                    </td>
                    <td>


                        <form action="{{ route('guru.destroy',['guru'=> $guru->id]) }}" method="POST"
                            id="deleteGuru-{{ $guru->id }}">
                            @method('DELETE')
                            @csrf
                        </form>
                        <a href="{{ route('guru.show',['guru'=> $guru->id]) }} " class="btn btn-success
                            btn-sm">Detail</a>
                        <a href="{{ route('guru.edit',['guru' => $guru->id]) }}" class="btn btn-warning btn-sm">Edit</a>
                        <button type="button" onclick="deleteItem({{ $guru->id }})"
                            class="btn btn-danger btn-sm">Delete</button>
                    </td>
                </tr>

                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('script')

<script>
    $(() =>{
        $('#tabelGuru').DataTable();
    })

    function deleteItem(id){
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
            if (result.isConfirmed) {
                $('#deleteGuru-'+id).get(0).submit();
            }
        })
    }
</script>
@endsection